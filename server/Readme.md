Создание миграции

``` bash
migrate create -ext sql -dir ./schema -seq init
```

Применение

``` bash
migrate -path ./schema -database 'postgres://postgres:qwerty@localhost:5432/postgres?sslmode=disable' up
```