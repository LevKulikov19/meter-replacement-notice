package main

import (
	"os"
	"sync"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"gitlab.com/LevKulikov19/meter-replacement-notice/server"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/handler"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/repository"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/service"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/telegram"

	_ "github.com/lib/pq"
)

func main() {
	if err := initConfig(); err != nil {
		logrus.Fatalf("error initializing configs: %s", err.Error())
	}

	if err := godotenv.Load(); err != nil {
		logrus.Fatalf("error loading env variables: %s", err.Error())
	}

	db, err := repository.NewPostgresDB(repository.Config{
		Host:     os.Getenv("DB_HOST"),
		Port:     viper.GetString("db.port"),
		Username: viper.GetString("db.username"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	if err != nil {
		logrus.Fatalf("failed to initialize db: %s", err.Error())
	}

	repositories := repository.NewRepository(db)
	services := service.NewService(repositories)
	handlers := handler.NewHandler(services)

	srv := new(server.Server)

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		defer wg.Done()
		if err := srv.Run(viper.GetString("port"), handlers.InitRoutes()); err != nil {
			logrus.Fatalf("error occured while running http server: %s", err.Error())
		}
	}()

	go func() {
		defer wg.Done()
		runBot(repositories)
	}()

	wg.Wait()
}

func runBot(repositories *repository.Repository) {
	telegram.SetRepo(repositories)
	bot, err := tgbotapi.NewBotAPI(os.Getenv("TG_TOKEN"))
	if err != nil {
		logrus.Panic(err)
	}

	bot.Debug = true

	logrus.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := bot.GetUpdatesChan(u)

	go func() {
		ticker := time.NewTicker(10 * time.Second)
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				telegram.SendNotification(bot)
			}
		}
	}()

	for update := range updates {
		if update.CallbackQuery != nil {
			telegram.ParseCallback(bot, update)
		}
		if update.Message != nil {
			telegram.ParseMessage(bot, update)
		}
	}

}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
