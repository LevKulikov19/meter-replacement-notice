package telegram

import (
	"fmt"
	"strconv"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/sirupsen/logrus"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/repository"
)

var repositories *repository.Repository

func SetRepo(repo *repository.Repository) {
	repositories = repo
}

type Notification struct {
	IdUserTG int64
	Message  string
}

func SendNotification(bot *tgbotapi.BotAPI) {
	notifications, _ := repositories.Notification.GetUserNotificationsByDate(time.Now())
	for _, notification := range notifications {
		message := fmt.Sprintf("Уважаемый пользователь, обратите внимание, что счетчик %s (%s) в объекте недвижимости '%s' (%s) с заводским номером %s, изготовленный %s и проверенный %s, скоро потребует замены. Пожалуйста, примите соответствующие меры.",
			notification.MeterType, notification.PartNumber, notification.ObjectName, notification.ObjectDescription, notification.PartNumber, notification.DateManufacture.Format("02-01-2006"), notification.DateVerification.Format("02-01-2006"))
		msg := tgbotapi.NewMessage(notification.TGId, message)

		// Добавляем кнопку
		button := tgbotapi.NewInlineKeyboardButtonData("Прочитал, понял, сделаю!", "read_notification_id_"+strconv.Itoa(notification.Id))
		keyboard := tgbotapi.NewInlineKeyboardMarkup(tgbotapi.NewInlineKeyboardRow(button))
		msg.ReplyMarkup = keyboard

		_, err := bot.Send(msg)
		if err != nil {
			logrus.Panic(err)
		}
	}
}

func ParseCallback(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	if update.CallbackQuery.Data[:len("read_notification_id_")] == "read_notification_id_" {
		notifyId, _ := strconv.Atoi(update.CallbackQuery.Data[len("read_notification_id_"):])
		err := repositories.Notification.SetReadState(notifyId)
		if err != nil {
			logrus.Printf("Telegram, not set read notification id: %d", notifyId)
		}
		msg := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, "Спасибо за подтверждение! ♥️")
		bot.Send(msg)
		callbackConf := tgbotapi.NewCallback(update.CallbackQuery.ID, "")
		bot.Send(callbackConf)
	}
}

func ParseMessage(bot *tgbotapi.BotAPI, update tgbotapi.Update) {
	if update.Message.IsCommand() {
		switch update.Message.Command() {
		case "start":
			msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Пожалуйста, введите код для входа (данный бот принимает от пользователя только код для входа):")
			bot.Send(msg)
		}
	} else {
		if !authTGUser(update.SentFrom().ID) {
			code, err := strconv.Atoi(update.Message.Text)
			if err != nil {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Код неверный. Пожалуйста, попробуйте еще раз.")
				bot.Send(msg)
				return
			}
			id, _ := repositories.GetUserIdByTGCode(code)
			if id != -1 {
				userID, err := repositories.GetUserIdByTGCode(code)
				if err != nil {
					msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Внутренняя ошибка сервера. Напишите в техническую поддержку")
					bot.Send(msg)
					return
				}
				repositories.PutUserTGId(userID, update.SentFrom().ID)
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Код верный! Добро пожаловать!")
				bot.Send(msg)
			} else {
				msg := tgbotapi.NewMessage(update.Message.Chat.ID, "Код неверный. Пожалуйста, попробуйте еще раз.")
				bot.Send(msg)
			}
		}

	}
}

func authTGUser(code int64) bool {
	res, err := repositories.CheckUserHasTGId(code)
	if err != nil {
		logrus.Printf("Telegram send code invalid: %d", code)
	}
	return res
}
