package model

type UserApp struct {
	Id       int    `json:"-" db:"id"`
	Login    string `json:"login"`
	Password string `json:"password"`
	TGId     int64  `json:"tgId"`
}

type UserToken struct {
	Id           int    `json:"-" db:"id"`
	RefreshToken string `json:"refreshToken"`
	TGCode       int    `json:"tgCode"`
}
