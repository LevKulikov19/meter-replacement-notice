package model

import (
	"time"
)

type RealEstateObject struct {
	Id          int    `json:"id"`
	UserId      int    `json:"user"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Electric    Meter  `json:"electric"`
	Gas         Meter  `json:"gas"`
	Water       Meter  `json:"water"`
	WaterHot    Meter  `json:"waterHot"`
}

type Meter struct {
	Id               int       `json:"id"`
	Active           bool      `json:"active"`
	Name             string    `json:"name"`
	PartNumber       string    `json:"numberMeter"`
	DateManufacture  time.Time `json:"dateManufacture"`
	DateVerification time.Time `json:"dateVerification"`
}

type RealEstateObjectInput struct {
	Name        string     `json:"name"`
	Description string     `json:"description"`
	Electric    MeterInput `json:"electric"`
	Gas         MeterInput `json:"gas"`
	Water       MeterInput `json:"water"`
	WaterHot    MeterInput `json:"waterHot"`
}

type MeterInput struct {
	Active           bool      `json:"active"`
	Name             string    `json:"name"`
	PartNumber       string    `json:"numberMeter"`
	DateManufacture  time.Time `json:"dateManufacture"`
	DateVerification time.Time `json:"dateVerification"`
}

type RealEstateObjectCompact struct {
	Id                       int       `json:"id"`
	Name                     string    `json:"name"`
	Description              string    `json:"description"`
	Electric                 bool      `json:"electric"`
	ElectricDateVerification time.Time `json:"electricDateVerification"`
	Gas                      bool      `json:"gas"`
	GasDateVerification      time.Time `json:"gasDateVerification"`
	Water                    bool      `json:"water"`
	WaterDateVerification    time.Time `json:"waterDateVerification"`
	WaterHot                 bool      `json:"waterHot"`
	WaterHotDateVerification time.Time `json:"waterHotDateVerification"`
}

type NotificationMeterOutdate struct {
	Id        int
	MeterId   int
	DateSand  time.Time
	ReadState bool
}

type NotificationMeterOutdateInput struct {
	MeterId   int
	DateSand  time.Time
	ReadState bool
}

type UserNotification struct {
	Id                int
	TGId              int64     `db:"TGId"`
	DateSand          time.Time `db:"DateSand"`
	ObjectName        string
	ObjectDescription string
	MeterType         string
	PartNumber        string
	DateManufacture   time.Time
	DateVerification  time.Time
}
