package handler

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/service"
)

type Handler struct {
	services *service.Service
}

func NewHandler(services *service.Service) *Handler {
	return &Handler{services: services}
}

func (h *Handler) InitRoutes() *gin.Engine {
	router := gin.New()
	router.Use(CORSMiddleware())

	auth := router.Group("/auth")
	{
		auth.POST("/signup", h.signUp)
		auth.POST("/signin", h.signIn)
		auth.POST("/refresh", h.refresh)
		auth.POST("/logout", h.userIdentity, h.logout)
	}

	object := router.Group("/object", h.userIdentity)
	{
		object.GET("/list", h.getObjectList)
		object.POST("/", h.createObject)
		object.GET("/:id", h.getObject)
		object.PUT("/", h.putObject)
		object.DELETE("/:id", h.deleteObject)
	}

	return router
}
