package repository

import (
	"database/sql"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
)

type AuthPostgres struct {
	db *sqlx.DB
}

func NewAuthPostgres(db *sqlx.DB) *AuthPostgres {
	return &AuthPostgres{db: db}
}

func (r *AuthPostgres) CreateUser(user model.UserApp) (int, error) {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (Login, Password) VALUES ($1, $2) RETURNING Id", userAppTable)
	row := r.db.QueryRow(query, user.Login, user.Password)
	if err := row.Scan(&id); err != nil {
		return 0, err
	}
	return id, nil
}

func (r *AuthPostgres) GetUser(login, password string) (model.UserApp, error) {
	var user model.UserApp

	query := fmt.Sprintf("SELECT Id FROM %s WHERE login=$1 AND password=$2", userAppTable)
	err := r.db.Get(&user, query, login, password)

	return user, err
}

func (r *AuthPostgres) PutUserRefreshToken(userId int, refreshToken string, tgCode string) error {
	var id int
	query := fmt.Sprintf("INSERT INTO %s (UserAppId, RefreshToken, TGCode) VALUES ($1, $2, $3) RETURNING Id", userTokenTable)
	var row *sql.Row
	if tgCode != "" {
		row = r.db.QueryRow(query, userId, refreshToken, tgCode)
	} else {
		row = r.db.QueryRow(query, userId, refreshToken, nil)
	}

	if err := row.Scan(&id); err != nil {
		return err
	}

	return nil
}

func (r *AuthPostgres) DeleteUserRefreshToken(userId int) error {
	query := fmt.Sprintf("DELETE FROM %s WHERE UserAppId = $1", userTokenTable)
	_, err := r.db.Exec(query, userId)
	if err != nil {
		return err
	}
	return nil
}

func (r *AuthPostgres) GetUserRefreshToken(refreshToken string) (int, error) {
	query := fmt.Sprintf("SELECT UserAppId FROM %s WHERE RefreshToken = $1", userTokenTable)
	row := r.db.QueryRow(query, refreshToken)
	var id int
	if err := row.Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

func (r *AuthPostgres) CheckLogin(login string) (bool, error) {
	query := fmt.Sprintf("SELECT * FROM %s WHERE Login = $1", userAppTable)

	row := r.db.QueryRow(query, login)
	user := &model.UserApp{}

	err := row.Scan(&user.Id, &user.Login, &user.Password)
	if err != nil {
		if err == sql.ErrNoRows {
			return false, nil
		}
		return false, err
	}

	return true, nil
}

func (r *AuthPostgres) GetUserIdByTGCode(tgCode int) (int, error) {
	query := "SELECT UserApp.Id FROM UserApp INNER JOIN UserToken ON UserApp.Id = UserToken.UserAppId WHERE UserToken.TGCode = $1"
	row := r.db.QueryRow(query, tgCode)
	var id int
	if err := row.Scan(&id); err != nil {
		return -1, err
	}
	return id, nil
}

func (r *AuthPostgres) PutUserTGId(userId int, tgId int64) error {
	query := "UPDATE UserApp SET TGId = $2 WHERE Id = $1"
	row := r.db.QueryRow(query, userId, tgId)
	var id int
	if err := row.Scan(&id); err != nil {
		return err
	}
	return nil
}

func (r *AuthPostgres) CheckUserHasTGId(tgId int64) (bool, error) {
	query := "SELECT COUNT(*) FROM UserApp WHERE TGId = $1"
	row := r.db.QueryRow(query, tgId)
	var count int
	if err := row.Scan(&count); err != nil {
		return false, err
	}
	return count > 0, nil
}

func (r *AuthPostgres) CheckUserHasTGIdById(userId int) (bool, error) {
	query := "SELECT COUNT(TGId) FROM UserApp WHERE Id = $1"
	row := r.db.QueryRow(query, userId)
	var id int = 0
	if err := row.Scan(&id); err != nil {
		return false, err
	}
	return id > 0, nil
}
