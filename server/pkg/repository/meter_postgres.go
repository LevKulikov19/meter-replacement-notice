package repository

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
)

type MeterPostgres struct {
	db           *sqlx.DB
	notification *NotificationPostgres
}

func NewMeterPostgres(db *sqlx.DB, notify *NotificationPostgres) *MeterPostgres {
	return &MeterPostgres{db: db, notification: notify}
}

func (r *MeterPostgres) CreateMeter(meter model.Meter) (int, error) {
	var id int

	query := fmt.Sprintf(
		`INSERT INTO %s (Active, Name, DateManufacture, DateVerification, PartNumber) VALUES ($1, $2, $3, $4, $5) RETURNING Id`,
		meterTable)
	row := r.db.QueryRow(query, meter.Active, meter.Name, meter.DateManufacture, meter.DateVerification, meter.PartNumber)
	err := row.Scan(&id)

	if err != nil {
		return 0, err
	}
	meter.Id = id
	if meter.Active {
		_, err = r.notification.CreateNotification(makeNotification(meter))
	}

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *MeterPostgres) GetMeter(id int) (model.Meter, error) {
	var meter model.Meter

	query := fmt.Sprintf(`SELECT * FROM %s WHERE Id=$1`, meterTable)
	err := r.db.Get(&meter, query, id)

	return meter, err
}

func (r *MeterPostgres) PutMeter(meter model.Meter) error {
	meterOld, _ := r.GetMeter(meter.Id)
	query := fmt.Sprintf(`UPDATE %s SET Active=$2, Name=$3, DateManufacture=$4, DateVerification=$5, PartNumber=$6 WHERE Id=$1`, meterTable)
	_, err := r.db.Exec(query, meter.Id, meter.Active, meter.Name, meter.DateManufacture, meter.DateVerification, meter.PartNumber)
	if meter.Active {
		notify, _ := r.notification.GetNotificationByMeter(meter.Id)
		notify.DateSand = meter.DateVerification.AddDate(0, -6, 0)
		r.notification.PutNotification(notify)
	}

	if meter.Active != meterOld.Active {
		if meter.Active {
			r.notification.CreateNotification(makeNotification(meter))
		} else {
			notify, _ := r.notification.GetNotificationByMeter(meter.Id)
			r.notification.DeleteNotification(notify.Id)
		}
	}
	return err
}

func (r *MeterPostgres) DeleteMeter(meterId int) (int, error) {
	query := fmt.Sprintf(`DELETE FROM %s WHERE Id=$1 RETURNING Id`, meterTable)
	row := r.db.QueryRow(query, meterId)

	var id int
	err := row.Scan(&id)

	if err != nil {
		return 0, err
	}

	notify, _ := r.notification.GetNotificationByMeter(meterId)
	r.notification.DeleteNotification(notify.Id)

	return id, nil
}

func makeNotification(meter model.Meter) model.NotificationMeterOutdateInput {
	var notify model.NotificationMeterOutdateInput
	notify.MeterId = meter.Id
	notify.ReadState = false
	notify.DateSand = meter.DateVerification.AddDate(0, -6, 0)
	return notify
}
