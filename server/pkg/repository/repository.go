package repository

import (
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
)

type Authorization interface {
	CreateUser(user model.UserApp) (int, error)
	GetUser(username string, password string) (model.UserApp, error)
	PutUserRefreshToken(userId int, refreshToken string, tgCode string) error
	DeleteUserRefreshToken(userId int) error
	GetUserRefreshToken(refreshToken string) (int, error)
	CheckLogin(login string) (bool, error)
	GetUserIdByTGCode(tgCode int) (int, error)
	PutUserTGId(userId int, tgId int64) error
	CheckUserHasTGId(tgId int64) (bool, error)
	CheckUserHasTGIdById(userId int) (bool, error)
}

type ObjectList interface {
	GetObjectList(userId int) ([]model.RealEstateObject, error)
}

type Object interface {
	CreateObject(object model.RealEstateObject) (int, error)
	GetObject(id int) (model.RealEstateObject, error)
	PutObject(object model.RealEstateObject) error
	DeleteObject(object model.RealEstateObject) (int, error)
	GetObjectCompactList(userId int) ([]model.RealEstateObjectCompact, error)
}

type Meter interface {
	CreateMeter(meter model.Meter) (int, error)
	GetMeter(id int) (model.Meter, error)
	PutMeter(meter model.Meter) error
	DeleteMeter(meterId int) (int, error)
}

type Notification interface {
	CreateNotification(notification model.NotificationMeterOutdateInput) (int, error)
	GetNotification(id int) (model.NotificationMeterOutdate, error)
	PutNotification(notification model.NotificationMeterOutdate) error
	DeleteNotification(meterId int) (int, error)
	GetNotificationByMeter(meterID int) (model.NotificationMeterOutdate, error)
	GetUserNotificationsByDate(date time.Time) ([]model.UserNotification, error)
	SetReadState(notificationId int) error
}

type Repository struct {
	Authorization
	ObjectList
	Object
	Meter
	Notification
}

func NewRepository(db *sqlx.DB) *Repository {
	var notify = NewNotificationPostgres(db)
	var meter = NewMeterPostgres(db, notify)
	return &Repository{
		Authorization: NewAuthPostgres(db),
		Meter:         meter,
		Object:        NewObjectPostgres(db, meter),
		Notification:  notify,
	}
}
