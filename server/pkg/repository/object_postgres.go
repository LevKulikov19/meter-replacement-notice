package repository

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
)

type ObjectPostgres struct {
	db    *sqlx.DB
	meter *MeterPostgres
}

func NewObjectPostgres(db *sqlx.DB, meter *MeterPostgres) *ObjectPostgres {
	return &ObjectPostgres{db: db, meter: meter}
}

func (r *ObjectPostgres) CreateObject(object model.RealEstateObject) (int, error) {
	query := `INSERT INTO RealEstateObject (UserId, Name, Description, Electric, Gas, Water, WaterHot) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING id`
	var id int
	err := r.db.QueryRow(query, object.UserId, object.Name, object.Description,
		object.Electric.Id, object.Gas.Id, object.Water.Id, object.WaterHot.Id).Scan(&id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (r *ObjectPostgres) GetObject(id int) (model.RealEstateObject, error) {
	var obj model.RealEstateObject

	query := `SELECT Id, UserId, Name, Description, Electric, Gas, Water, WaterHot FROM RealEstateObject WHERE Id = $1`
	row := r.db.QueryRow(query, id)

	var userId int
	var name string
	var description string
	var electric int
	var gas int
	var water int
	var waterHot int

	err := row.Scan(&obj.Id, &userId, &name, &description, &electric, &gas, &water, &waterHot)
	if err != nil {
		return obj, err
	}

	obj.UserId = userId
	obj.Name = name
	obj.Description = description

	obj.Electric, err = r.meter.GetMeter(electric)
	if err != nil {
		return obj, err
	}

	obj.Gas, err = r.meter.GetMeter(gas)
	if err != nil {
		return obj, err
	}

	obj.Water, err = r.meter.GetMeter(water)
	if err != nil {
		return obj, err
	}

	obj.WaterHot, err = r.meter.GetMeter(waterHot)
	if err != nil {
		return obj, err
	}

	return obj, nil
}

func (r *ObjectPostgres) PutObject(object model.RealEstateObject) error {
	err := r.meter.PutMeter(object.Electric)
	if err != nil {
		return err
	}

	err = r.meter.PutMeter(object.Gas)
	if err != nil {
		return err
	}

	err = r.meter.PutMeter(object.Water)
	if err != nil {
		return err
	}

	err = r.meter.PutMeter(object.WaterHot)
	if err != nil {
		return err
	}

	query := `UPDATE RealEstateObject SET UserId = $1, Name = $2, Description = $3, Electric = $4, Gas = $5, Water = $6, WaterHot = $7 WHERE Id = $8`
	_, err = r.db.Exec(query, object.UserId, object.Name, object.Description, object.Electric.Id, object.Gas.Id, object.Water.Id, object.WaterHot.Id, object.Id)
	if err != nil {
		return err
	}
	return nil
}

func (r *ObjectPostgres) DeleteObject(object model.RealEstateObject) (int, error) {

	query := `DELETE FROM RealEstateObject WHERE Id = $1 RETURNING id`
	var id int
	err := r.db.QueryRow(query, object.Id).Scan(&id)
	if err != nil {
		return 0, err
	}

	_, err = r.meter.DeleteMeter(object.Electric.Id)
	if err != nil {
		return 0, err
	}

	_, err = r.meter.DeleteMeter(object.Gas.Id)
	if err != nil {
		return 0, err
	}

	_, err = r.meter.DeleteMeter(object.Water.Id)
	if err != nil {
		return 0, err
	}

	_, err = r.meter.DeleteMeter(object.WaterHot.Id)
	if err != nil {
		return 0, err
	}
	return id, nil
}

func (r *ObjectPostgres) GetObjectCompactList(userId int) ([]model.RealEstateObjectCompact, error) {
	var objects []model.RealEstateObjectCompact
	query :=
		`SELECT
			RealEstateObject.Id,
			RealEstateObject.Name,
			RealEstateObject.Description,
			ElectricMeter.Active AS ElectricActive,
			ElectricMeter.DateVerification AS ElectricDate,
			GasMeter.Active AS GasActive,
			GasMeter.DateVerification AS GasDate,
			WaterMeter.Active AS WaterActive,
			WaterMeter.DateVerification AS WaterDate,
			WaterHotMeter.Active AS WaterHotActive,
			WaterHotMeter.DateVerification AS WaterHotDate
		FROM
			RealEstateObject
			LEFT JOIN
				Meter AS ElectricMeter ON RealEstateObject.Electric = ElectricMeter.Id
			LEFT JOIN
				Meter AS GasMeter ON RealEstateObject.Gas = GasMeter.Id
			LEFT JOIN
				Meter AS WaterMeter ON RealEstateObject.Water = WaterMeter.Id
			LEFT JOIN
				Meter AS WaterHotMeter ON RealEstateObject.WaterHot = WaterHotMeter.Id
			WHERE userId = $1`
	rows, err := r.db.Query(query, userId)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var obj model.RealEstateObjectCompact
		err = rows.Scan(&obj.Id, &obj.Name, &obj.Description,
			&obj.Electric, &obj.ElectricDateVerification,
			&obj.Gas, &obj.GasDateVerification,
			&obj.Water, &obj.WaterDateVerification,
			&obj.WaterHot, &obj.WaterHotDateVerification)
		if err != nil {
			return nil, err
		}

		objects = append(objects, obj)
	}

	if err = rows.Err(); err != nil {
		return nil, err
	}

	return objects, nil
}
