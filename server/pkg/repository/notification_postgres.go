package repository

import (
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
)

type NotificationPostgres struct {
	db *sqlx.DB
}

func NewNotificationPostgres(db *sqlx.DB) *NotificationPostgres {
	return &NotificationPostgres{db: db}
}

func (r *NotificationPostgres) CreateNotification(notification model.NotificationMeterOutdateInput) (int, error) {
	var id int

	query := fmt.Sprintf(
		`INSERT INTO %s (MeterId, DateSand, ReadState) VALUES ($1, $2, $3) RETURNING Id`,
		"notificationmeteroutdate")
	row := r.db.QueryRow(query, notification.MeterId, notification.DateSand, notification.ReadState).Scan(&id)
	err := row

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *NotificationPostgres) GetNotification(id int) (model.NotificationMeterOutdate, error) {
	var notification model.NotificationMeterOutdate

	query := fmt.Sprintf(`SELECT * FROM %s WHERE Id=$1`, "NotificationMeterOutdate")
	err := r.db.Get(&notification, query, id)

	return notification, err
}

func (r *NotificationPostgres) PutNotification(notification model.NotificationMeterOutdate) error {
	query := fmt.Sprintf(`UPDATE %s SET MeterId=$2, DateSand=$3, ReadState=$4 WHERE Id=$1`, "NotificationMeterOutdate")
	_, err := r.db.Exec(query, notification.Id, notification.MeterId, notification.DateSand, notification.ReadState)

	return err
}

func (r *NotificationPostgres) DeleteNotification(meterId int) (int, error) {
	query := fmt.Sprintf(`DELETE FROM %s WHERE Id=$1 RETURNING Id`, "NotificationMeterOutdate")
	row := r.db.QueryRow(query, meterId)

	var id int
	err := row.Scan(&id)

	if err != nil {
		return 0, err
	}

	return id, nil
}

func (r *NotificationPostgres) GetNotificationByMeter(meterID int) (model.NotificationMeterOutdate, error) {
	var notification model.NotificationMeterOutdate

	query := fmt.Sprintf(`SELECT * FROM %s WHERE MeterId=$1`, "NotificationMeterOutdate")
	err := r.db.Get(&notification, query, meterID)
	return notification, err
}

func (r *NotificationPostgres) GetUserNotificationsByDate(date time.Time) ([]model.UserNotification, error) {
	var userNotifications []model.UserNotification

	query := `SELECT
				NotificationMeterOutdate.Id AS NotificationID,
				UserApp.TGId,
				NotificationMeterOutdate.DateSand,
				RealEstateObject.Name AS ObjectName,
				RealEstateObject.Description AS ObjectDescription,
				Meter.Name AS MeterType,
				Meter.PartNumber,
				Meter.DateManufacture,
				Meter.DateVerification
			FROM
				UserApp
			JOIN
				RealEstateObject ON UserApp.Id = RealEstateObject.UserId
			JOIN
				NotificationMeterOutdate
					ON RealEstateObject.Electric = NotificationMeterOutdate.MeterId
					OR RealEstateObject.Gas = NotificationMeterOutdate.MeterId
					OR RealEstateObject.Water = NotificationMeterOutdate.MeterId
					OR RealEstateObject.WaterHot = NotificationMeterOutdate.MeterId
			JOIN
				Meter
					ON RealEstateObject.Electric = Meter.Id
					OR RealEstateObject.Gas = Meter.Id
					OR RealEstateObject.Water = Meter.Id
					OR RealEstateObject.WaterHot = Meter.Id
			WHERE
					DATE(NotificationMeterOutdate.DateSand) <= DATE(NOW())
				AND NotificationMeterOutdate.ReadState = FALSE
				AND Meter.Active = TRUE`

	rows, err := r.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var notify model.UserNotification
		err = rows.Scan(
			&notify.Id,
			&notify.TGId,
			&notify.DateSand,
			&notify.ObjectName,
			&notify.ObjectDescription,
			&notify.MeterType,
			&notify.PartNumber,
			&notify.DateManufacture,
			&notify.DateVerification)
		if err != nil {
			return nil, err
		}

		userNotifications = append(userNotifications, notify)
	}

	return userNotifications, err
}

func (r *NotificationPostgres) SetReadState(notificationId int) error {
	query := `UPDATE NotificationMeterOutdate SET ReadState = true WHERE Id = $1`

	_, err := r.db.Exec(query, notificationId)
	if err != nil {
		return err
	}

	return nil
}
