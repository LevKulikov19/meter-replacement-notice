package service

import (
	"errors"

	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/repository"
)

type ObjectService struct {
	repoObj   repository.Object
	repoMeter repository.Meter
}

func NewObjectService(repoObj repository.Object, repoMeter repository.Meter) *ObjectService {
	return &ObjectService{repoObj: repoObj, repoMeter: repoMeter}
}

func (s *ObjectService) CreateObject(object model.RealEstateObjectInput, userId int) (int, error) {
	var realObject model.RealEstateObject

	realObject.Id = 0
	realObject.Description = object.Description
	realObject.Name = object.Name
	realObject.UserId = userId

	var realElectricMeter model.Meter
	realElectricMeter.Id = 0
	realElectricMeter.Active = object.Electric.Active
	realElectricMeter.Name = object.Electric.Name
	realElectricMeter.PartNumber = object.Electric.PartNumber
	realElectricMeter.DateManufacture = object.Electric.DateManufacture
	realElectricMeter.DateVerification = object.Electric.DateVerification
	meterId, err := s.repoMeter.CreateMeter(realElectricMeter)
	if err != nil {
		return 0, errors.New("electric meter do not create")
	}
	meter, _ := s.repoMeter.GetMeter(meterId)
	realObject.Electric = meter

	var realGasMeter model.Meter
	realGasMeter.Id = 0
	realGasMeter.Active = object.Gas.Active
	realGasMeter.Name = object.Gas.Name
	realGasMeter.PartNumber = object.Gas.PartNumber
	realGasMeter.DateManufacture = object.Gas.DateManufacture
	realGasMeter.DateVerification = object.Gas.DateVerification
	meterId, err = s.repoMeter.CreateMeter(realGasMeter)
	if err != nil {
		return 0, errors.New("gas meter do not create")
	}
	meter, _ = s.repoMeter.GetMeter(meterId)
	realObject.Gas = meter

	var realWaterMeter model.Meter
	realWaterMeter.Id = 0
	realWaterMeter.Active = object.Water.Active
	realWaterMeter.Name = object.Water.Name
	realWaterMeter.PartNumber = object.Water.PartNumber
	realWaterMeter.DateManufacture = object.Water.DateManufacture
	realWaterMeter.DateVerification = object.Water.DateVerification
	meterId, err = s.repoMeter.CreateMeter(realWaterMeter)
	if err != nil {
		return 0, errors.New("water meter do not create")
	}
	meter, _ = s.repoMeter.GetMeter(meterId)
	realObject.Water = meter

	var realWaterHotMeter model.Meter
	realWaterHotMeter.Id = 0
	realWaterHotMeter.Active = object.WaterHot.Active
	realWaterHotMeter.Name = object.WaterHot.Name
	realWaterHotMeter.PartNumber = object.WaterHot.PartNumber
	realWaterHotMeter.DateManufacture = object.WaterHot.DateManufacture
	realWaterHotMeter.DateVerification = object.WaterHot.DateVerification
	meterId, err = s.repoMeter.CreateMeter(realWaterHotMeter)
	if err != nil {
		return 0, errors.New("water hot meter do not create")
	}
	meter, _ = s.repoMeter.GetMeter(meterId)
	realObject.WaterHot = meter

	return s.repoObj.CreateObject(realObject)
}

func (s *ObjectService) GetObject(objectId int) (model.RealEstateObject, error) {
	return s.repoObj.GetObject(objectId)
}

func (s *ObjectService) PutObject(object model.RealEstateObject, userId int) error {
	object.UserId = userId
	return s.repoObj.PutObject(object)
}

func (s *ObjectService) DeleteObject(objectId int, userId int) (int, error) {
	obj, err := s.repoObj.GetObject(objectId)
	if err != nil {
		return 0, errors.New("object not found")
	}
	return s.repoObj.DeleteObject(obj)
}

func (s *ObjectService) GetAllObject(userId int) ([]model.RealEstateObjectCompact, error) {
	return s.repoObj.GetObjectCompactList(userId)
}
