package service

import (
	"crypto/sha1"
	"errors"
	"fmt"
	"math/rand"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/repository"
)

const (
	salt            = "test"
	accessTokenTTL  = 1 * time.Hour
	refreshTokenTTL = 168 * time.Hour
	accessTokenKey  = "qrkjk#4#%35FSFJlja#4353KSFjH"
	refreshTokenKey = " refreshqrkjk#4#%35FSFJlja#4353KSFjH"
)

type tokenClaims struct {
	jwt.StandardClaims
	UserId int `json:"user_id"`
}

type AuthService struct {
	repo repository.Authorization
}

func NewAuthService(repo repository.Authorization) *AuthService {
	return &AuthService{repo: repo}
}

func (s *AuthService) CreateUser(user model.UserApp) (int, error) {
	loginExists, err := s.repo.CheckLogin(user.Login)
	if loginExists || err != nil {
		return 0, errors.New("логин занят, используйте другой")
	}

	user.Password = generatePasswordHash(user.Password)
	return s.repo.CreateUser(user)
}

func generatePasswordHash(password string) string {
	hash := sha1.New()
	hash.Write([]byte(password))

	return fmt.Sprintf("%x", hash.Sum([]byte(salt)))
}

func (s *AuthService) ParseToken(accessToken string) (int, error) {
	token, err := jwt.ParseWithClaims(accessToken, &tokenClaims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		}

		return []byte(accessTokenKey), nil
	})
	if err != nil {
		return 0, err
	}

	claims, ok := token.Claims.(*tokenClaims)
	if !ok {
		return 0, errors.New("token claims are not of type *tokenClaims")
	}

	return claims.UserId, nil
}

func (s *AuthService) GenerateToken(login string, password string) (string, string, string, error) {

	user, err := s.repo.GetUser(login, generatePasswordHash(password))
	if err != nil {
		return "", "", "", err
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(accessTokenTTL).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		user.Id,
	})

	refreshToken := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(refreshTokenTTL).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		user.Id,
	})

	tokenString, err := token.SignedString([]byte(accessTokenKey))
	if err != nil {
		return "", "", "", err
	}
	refreshTokenString, err := refreshToken.SignedString([]byte(refreshTokenKey))
	if err != nil {
		return "", "", "", err
	}

	existsAuthTG, err := s.repo.CheckUserHasTGIdById(user.Id)
	if err != nil {
		return "", "", "", err
	}

	var tgCode string = ""
	if !existsAuthTG {
		tgCode = generateRandomDigits(6)
		err = s.repo.PutUserRefreshToken(user.Id, refreshTokenString, tgCode)
	} else {
		err = s.repo.PutUserRefreshToken(user.Id, refreshTokenString, "")
	}
	if err != nil {
		return "", "", "", err
	}

	return tokenString, refreshTokenString, tgCode, nil
}

func (s *AuthService) GenerateNewAccessToken(refreshToken string) (string, error) {
	id, err := s.repo.GetUserRefreshToken(refreshToken)
	if err != nil {
		return "", err
	}

	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, &tokenClaims{
		jwt.StandardClaims{
			ExpiresAt: time.Now().Add(accessTokenTTL).Unix(),
			IssuedAt:  time.Now().Unix(),
		},
		id,
	})

	newTokenString, err := newToken.SignedString([]byte(accessTokenKey))
	if err != nil {
		return "", err
	}

	return newTokenString, nil
}

func (s *AuthService) DisableRefreshToken(userId int) error {
	err := s.repo.DeleteUserRefreshToken(userId)
	if err != nil {
		return err
	}
	return nil
}

func generateRandomDigits(n int) string {
	rand.Seed(time.Now().UnixNano())
	digits := "0123456789"
	result := make([]byte, n)
	for i := range result {
		result[i] = digits[rand.Intn(len(digits))]
	}
	return string(result)
}
