package service

import (
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/model"
	"gitlab.com/LevKulikov19/meter-replacement-notice/server/pkg/repository"
)

type Authorization interface {
	CreateUser(user model.UserApp) (int, error)
	GenerateToken(login string, password string) (string, string, string, error)
	ParseToken(token string) (int, error)
	GenerateNewAccessToken(refreshToken string) (string, error)
	DisableRefreshToken(userId int) error
}

type Object interface {
	CreateObject(object model.RealEstateObjectInput, userId int) (int, error)
	GetObject(objectId int) (model.RealEstateObject, error)
	PutObject(object model.RealEstateObject, userId int) error
	DeleteObject(objectId int, userId int) (int, error)
	GetAllObject(userId int) ([]model.RealEstateObjectCompact, error)
}

type Service struct {
	Authorization
	Object
}

func NewService(repos *repository.Repository) *Service {
	return &Service{
		Authorization: NewAuthService(repos.Authorization),
		Object:        NewObjectService(repos.Object, repos.Meter),
	}
}
