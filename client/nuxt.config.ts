// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ["@nuxt/ui", "@nuxt/image", "nuxt-icons", "@pinia/nuxt",
  '@pinia-plugin-persistedstate/nuxt'
  ],
  image: {
    dir: 'assets',
  },
  colorMode: {
    preference: 'light',
  },
  build: {
    transpile: ['jsonwebtoken', 'axios']
  },
  routeRules: {
    '/object': {
      redirect: '/object/new'
    }
  }

})
