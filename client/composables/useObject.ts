import axios from 'axios';
import meterState from '~/utils/meterState';

type RealEstateObjectItem = {
  id: number
  name: string;
  description: string;
  electric: boolean;
  electricDateVerification: string;
  gas: boolean;
  gasDateVerification: string;
  water: boolean;
  waterDateVerification: string;
  waterHot: boolean;
  waterHotDateVerification: string;
};

type RealEstateObjectItemCompact = {
  id: number,
  name: string;
  description: string;
  electric: meterState;
  gas: meterState;
  water: meterState;
  waterHot: meterState;
};

function transformHouseToCompact(houses: RealEstateObjectItem[]): RealEstateObjectItemCompact[] {
  const currentDate = new Date();
  if (houses === null) return []
  return houses.map(house => {
      const electricStatus = house.electric ? meterState.on : meterState.off;
      const electricDateVerification = new Date(house.electricDateVerification);
      const isElectricOverdue = electricDateVerification <= currentDate ? meterState.overdue : electricStatus;

      const gasStatus = house.gas ? meterState.on : meterState.off;
      const gasDateVerification = new Date(house.gasDateVerification);
      const isGasOverdue = gasDateVerification <= currentDate ? meterState.overdue : gasStatus;

      const waterStatus = house.water ? meterState.on : meterState.off;
      const waterDateVerification = new Date(house.waterDateVerification);
      const isWaterOverdue = waterDateVerification <= currentDate ? meterState.overdue : waterStatus;

      const waterHotStatus = house.waterHot ? meterState.on : meterState.off;
      const waterHotDateVerification = new Date(house.waterHotDateVerification);
      const isWaterHotOverdue = waterHotDateVerification <= currentDate  ? meterState.overdue : waterHotStatus;

      return {
          id: house.id,
          name: house.name,
          description: house.description,
          electric: isElectricOverdue,
          gas: isGasOverdue,
          water: isWaterOverdue,
          waterHot: isWaterHotOverdue
      };
  });
}

interface HouseMeter {
  id: number;
  active: boolean;
  name: string;
  numberMeter: string;
  dateManufacture: Date;
  dateVerification: Date;
}

interface House {
  id: number;
  name: string;
  description: string;
  electric: HouseMeter;
  gas: HouseMeter;
  water: HouseMeter;
  waterHot: HouseMeter;
}

interface HouseMeterInput {
  active: boolean;
  name: string;
  numberMeter: string;
  dateManufacture: Date;
  dateVerification: Date;
}

interface HouseInput {
  name: string;
  description: string;
  electric: HouseMeterInput;
  gas: HouseMeterInput;
  water: HouseMeterInput;
  waterHot: HouseMeterInput;
}



export const useObject = () => {
  const { getToken } = useAuth()

  async function getListObject() {
    const token = await getToken();

    let response: any;
    try {
       response = await axios.get(`${import.meta.env.VITE_APP_BACKEND_BASE_URL}/object/list`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
    });
    }
    catch (e) {
      console.error(e)
    }
    return transformHouseToCompact(response.data);
  }
  async function getObject(id: number): Promise<House> {
    const token = await getToken();

    let response: any;
    try {
       response = await axios.get(`${import.meta.env.VITE_APP_BACKEND_BASE_URL}/object/${id}`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
    });
    }
    catch (e) {
    }
    return response.data as House
  }

  async function crateObject(object: HouseInput) {
    const token = await getToken();

    let obj: any = object;

    obj.electric.dateManufacture = object.electric.dateManufacture.toString()+"T00:00:00Z";
    obj.electric.dateVerification = object.electric.dateVerification.toString()+"T00:00:00Z";
    obj.gas.dateManufacture = object.gas.dateManufacture.toString()+"T00:00:00Z";
    obj.gas.dateVerification = object.gas.dateVerification.toString()+"T00:00:00Z";
    obj.water.dateManufacture = object.water.dateManufacture.toString()+"T00:00:00Z";
    obj.water.dateVerification = object.water.dateVerification.toString()+"T00:00:00Z";
    obj.waterHot.dateManufacture = object.waterHot.dateManufacture.toString()+"T00:00:00Z";
    obj.waterHot.dateVerification = object.waterHot.dateVerification.toString()+"T00:00:00Z";

    let response: any;
    try {
       response = await axios.post(`${import.meta.env.VITE_APP_BACKEND_BASE_URL}/object/`, obj, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
    });
    }
    catch (e) {
      console.error(e)
    }
    return
  }

  async function putObject(object: House) {
    const token = await getToken();

    let obj: any = object;

    obj.electric.dateManufacture = (new Date(object.electric.dateManufacture.toString())).toISOString();
    obj.electric.dateVerification = (new Date(object.electric.dateVerification.toString())).toISOString();

    obj.gas.dateManufacture = (new Date(object.gas.dateManufacture.toString())).toISOString();
    obj.gas.dateVerification = (new Date(object.gas.dateVerification.toString())).toISOString();

    obj.water.dateManufacture = (new Date(object.water.dateManufacture.toString())).toISOString();
    obj.water.dateVerification = (new Date(object.water.dateVerification.toString())).toISOString();

    obj.waterHot.dateManufacture = (new Date(object.waterHot.dateManufacture.toString())).toISOString();
    obj.waterHot.dateVerification = (new Date(object.waterHot.dateVerification.toString())).toISOString();

    let response: any;
    try {
       response = await axios.put(`${import.meta.env.VITE_APP_BACKEND_BASE_URL}/object/`, obj, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
    });
    }
    catch (e) {
    }
    return
  }

  async function deleteObject(id: number) {
    const token = await getToken();

    let response: any;
    try {
       response = await axios.delete(`${import.meta.env.VITE_APP_BACKEND_BASE_URL}/object/${id}`, {
        headers: {
          'Authorization': `Bearer ${token}`
        }
    });
    }
    catch (e) {
      console.error(e)
    }
    return
  }
  return {
    getListObject,
    getObject,
    crateObject,
    putObject,
    deleteObject
  }
}
