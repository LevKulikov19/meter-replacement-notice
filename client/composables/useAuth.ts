import axios from 'axios'
import { jwtDecode } from "jwt-decode";
import { useAuthStorage } from '~/stores/auth';

export const useAuth = () => {
  async function signup(Login: string, Password: string) {
    const url = `${import.meta.env.VITE_APP_BACKEND_BASE_URL}/auth/signup`
    const data = {
      Login,
      Password
    }

    try {
      await axios.post(url, data)
    } catch (error: any) {
      if (error.message === "Network Error")
        throw new Error("Ошибка сети")
      throw new Error( error.response.data.message || error.message)
    }
  }

  async function login(login: string, password: string) {
    const authStorage = useAuthStorage()
    const url = `${import.meta.env.VITE_APP_BACKEND_BASE_URL}/auth/signin`
    const data = {
      login,
      password
    }

    try {
      const response = await axios.post(url, data)
      authStorage.tgCode = response.data.tgCode;
      authStorage.accessToken = response.data.accessToken;
      authStorage.refreshToken = response.data.refreshToken;

      return response.data
    } catch (error: any) {
      throw new Error(error.message)
    }
  }

  async function refresh(refreshToken: string) {
    const authStorage = useAuthStorage()
    const url = `${import.meta.env.VITE_APP_BACKEND_BASE_URL}/auth/refresh`
    const data = {
      refreshToken
    }

    try {
      const response = await axios.post(url, data)
      authStorage.accessToken = response.data.accessToken
    } catch (error) {
      console.error(error)
    }

  }

  async function logout() {
    const authStorage = useAuthStorage()
    const url = `${import.meta.env.VITE_APP_BACKEND_BASE_URL}/auth/logout`
    const token = authStorage.accessToken;

    try {
      authStorage.accessToken = null
      authStorage.refreshToken = null
      const response = await axios.post(url, {
        headers: {
            'Authorization': `Bearer ${token}`
        }
      });
      return response.data
    } catch (error) {
      console.error(error)
    }
  }

  async function getToken() {
    const authStorage = useAuthStorage()
    const router = useRouter();
    let accessToken: string | null = authStorage.accessToken;
    const refreshToken: string | null = authStorage.refreshToken;
    if (accessToken === null || refreshToken === null) {
      router.push({ name: 'login' })
      return
    }
    let tokenData = accessToken !== undefined ? accessToken : "";
    const date: number | null = getJwtExpirationDate(tokenData);
    if (date === null) {
      throw Error("Ошибка в обработке токена")
    }
    if (Date.now() >= date) {
      try {
        if (refreshToken === undefined)
          throw Error("Ошибка в обработке токена")

        await refresh(refreshToken)
        accessToken = authStorage.accessToken
      } catch (e) {
        router.push({ name: 'login' })
        return
      }
    }
    return accessToken
  }

  function getJwtExpirationDate(token: string) {
    const decodedToken: any = jwtDecode(token)
    const { exp } = decodedToken

    if (!exp) {
      return null
    }

    return exp * 1000
  }

  const isAuth = () => {
    const authStorage = useAuthStorage()

    return authStorage.accessToken !== null;
  }

  return {
    signup,
    login,
    refresh,
    logout,
    getToken,
    getJwtExpirationDate,
    isAuth
  }
}