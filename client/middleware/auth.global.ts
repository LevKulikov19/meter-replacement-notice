export default defineNuxtRouteMiddleware((to, from) => {
    const { isAuth } = useAuth();
    const router = useRouter();
    if (isAuth() && to.name === 'login') {
        router.push({ name: 'realty' })
    }
    const ignorePage = [
        'index',
        'registration',
        'login'
    ]
    if (to.name && ignorePage.includes(String(to.name))) return;

    if (!isAuth())
        router.push({ name: 'login' })

})
