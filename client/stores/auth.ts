import { defineStore } from 'pinia';

export const useAuthStorage = defineStore('auth', {
    state: () => {
        return {
            tgCode: null,
            accessToken: null,
            refreshToken: null
        }
    },
    persist: {
        storage: persistedState.cookies,
    }
});
