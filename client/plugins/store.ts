export default defineNuxtPlugin(({ $pinia }) => {
    return {
      provide: {
        store: useAuthStorage($pinia),
      },
    };
  });